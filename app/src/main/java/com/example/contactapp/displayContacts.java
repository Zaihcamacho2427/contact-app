package com.example.contactapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

public class displayContacts extends AppCompatActivity {

    ListView lv_people;
    PersonAdapter adapter;
    Button b_display;
    AddressBook contactList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_contacts);


        lv_people = findViewById(R.id.lv_people);
        b_display = findViewById(R.id.b_display);


//        connect address book to existing address book using MyApplication
        contactList = MyApplication.getTheContactList();
//
        //create new adapter
        adapter = new PersonAdapter(displayContacts.this, contactList);
//
//        lv_people.setAdapter(new PersonAdapter(displayContacts.this, contactList));
//
//        adapter.notifyDataSetChanged();


//        DataService ds = new DataService(this);
//        contactList = ds.readList("addressBook.txt");


        b_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                DataService ds = new DataService(v.getContext());
//                contactList = ds.readList("addressBook.txt");
//                lv_people.setAdapter(new PersonAdapter(displayContacts.this, contactList));
//                //create new adapter
//                adapter = new PersonAdapter(displayContacts.this, contactList);
                lv_people.setAdapter(new PersonAdapter(displayContacts.this, contactList));
                adapter.notifyDataSetChanged();

            }
        });


        //click listener to edit a user
        lv_people.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editPerson(position);
            }
        });

    }



    public void editPerson(int position){

        BaseContact bc = contactList.getContactList().get(position);

        if (bc.getClass() == PersonContact.class) {
            Intent i = new Intent(getApplicationContext(), EditPersonContact.class);

            i.putExtra("edit", position);
            i.putExtra("fName", bc.getfName());
            i.putExtra("lName", bc.getlName());
            i.putExtra("pPN", bc.getPhoneNumber());
            i.putExtra("pEmail", bc.getEmail());
            i.putExtra("pPic", bc.getImageName());
            startActivity(i);
        }
        else if (bc.getClass() == BusinessContact.class) {

            Intent i = new Intent(getApplicationContext(), EditBusinessContact.class);
            i.putExtra("edit2", position);
            i.putExtra("fName", bc.getfName());
            i.putExtra("lName", bc.getlName());
            i.putExtra("bPN", bc.getPhoneNumber());
            i.putExtra("bEmail", bc.getEmail());
            i.putExtra("bPic", bc.getImageName());
            startActivity(i);
        }


    }

}
