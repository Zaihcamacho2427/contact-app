package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button b_displayContacts;
    Button b_addContact;
    Button b_searchContacts;
    TextView tv_typeOfContact;
    AddressBook ab;
    MyApplication ma;
//    AddressBook contactList = new AddressBook();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b_displayContacts = findViewById(R.id.b_displayContacts);
        b_addContact = findViewById(R.id.b_addContact);
        b_searchContacts = findViewById(R.id.b_searchContacts);
        tv_typeOfContact = findViewById(R.id.tv_typeOfContact);
        ab = ((MyApplication) this.getApplication()) .getTheContactList();



        //create on click listener for display contacts
        b_displayContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(v.getContext(), displayContacts.class);
                startActivity(i);

            }
        });

        b_addContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(v.getContext(), addQuestion.class);
                startActivity(i);
            }
        });
        b_searchContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), searchForm.class);
                startActivity(i);
            }
        });



    }



}
