package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

public class searchForm extends AppCompatActivity {


    EditText et_search;
    ListView lv_people;
    AddressBook contactList;
    AddressBook searchResults;
    PersonAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_form);

        et_search = findViewById(R.id.et_search);
        lv_people = findViewById(R.id.lv_people);

        //connect address book to existing address book using MyApplication
        contactList = ((MyApplication) this.getApplication()).getTheContactList();


//        //create new adapter
//        adapter = new PersonAdapter(searchForm.this, ab);
        //set list view to the adapter

      et_search.addTextChangedListener(new TextWatcher() {
          @Override
          public void beforeTextChanged(CharSequence s, int start, int count, int after) {

          }

          @Override
          public void onTextChanged(CharSequence s, int start, int before, int count) {
              //create new address book that will hold the results of contacts found after searched
              searchResults = contactList.searchForContact(s.toString());
              //set adapter to new address book made
              adapter = new PersonAdapter(searchForm.this, searchResults);
              //set the list view to the adapter
              lv_people.setAdapter(adapter);
              //notify adapter change
              adapter.notifyDataSetChanged();

//              if (s.toString() == ""){
//
//              }
          }

          @Override
          public void afterTextChanged(Editable s) {

          }
      });

        //click listener to edit a user
        lv_people.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editPerson(position);
            }
        });


//        //call address book function using search method and using searchedName as parameter
//        ab.searchForContact(searchedName);

//        b_search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                /*call address book function using search method and using searchedName as parameter
//                the string searchedName will look through the address book and see if first names or last names match the string
//                entered by the user.
//                if a contact contains the string entered, then the method will store that contact into an array and return
//                that array
//                */
//                ab.searchForContact(searchedName);
//                //ab (address book) now holds any and all contacts that contains the string entered by user
//                //set a new address book to the address book that contains the results from the search
//                newAB = ab;
//
////                for (int i = 0; i < newAB.getListSize(); i++){
////                    BaseContact bc = new BaseContact(newAB.getContact(i).getfName(), newAB.getContact(i).getlName()
////                    , newAB.getContact(i).getPhoneNumber(), newAB.getContact(i).getEmail(), newAB.getContact(i).getImageName());
////
////                }
//                //create intent to another page that will display the new address book that holds the results
//                Intent i = new Intent(v.getContext(), displaySearchedResults.class);
//                //send the address book to the other page so the list view on the other page can display the results
//                i.putStringArrayListExtra(newAB);
//                //start the activity
//                startActivity(i);
//
//            }
//        });


    }
    public void editPerson(int position){

        BaseContact bc = contactList.getContactList().get(position);

        if (bc.getClass() == PersonContact.class) {
            Intent i = new Intent(getApplicationContext(), EditPersonContact.class);

            i.putExtra("edit", position);
            i.putExtra("fName", bc.getfName());
            i.putExtra("lName", bc.getlName());
            i.putExtra("pPN", bc.getPhoneNumber());
            i.putExtra("pEmail", bc.getEmail());
            i.putExtra("pPic", bc.getImageName());
            startActivity(i);
        }
        else if (bc.getClass() == BusinessContact.class) {

            Intent i = new Intent(getApplicationContext(), EditBusinessContact.class);
            i.putExtra("edit2", position);
            i.putExtra("fName", bc.getfName());
            i.putExtra("lName", bc.getlName());
            i.putExtra("bPN", bc.getPhoneNumber());
            i.putExtra("bEmail", bc.getEmail());
            i.putExtra("bPic", bc.getImageName());
            startActivity(i);
        }


    }


}
