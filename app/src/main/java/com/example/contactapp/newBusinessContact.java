package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class newBusinessContact extends AppCompatActivity {

    Button b_add, b_cancel;
    EditText et_firstName;
    EditText et_lastName;
    EditText et_PN;
    EditText et_BusinessEmail;
    EditText et_pic;
    EditText et_businessHours;
    EditText et_businessWebsite;
    AddressBook contactList;
    PersonAdapter pa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_business_contact);

        b_add = findViewById(R.id.b_add);
        b_cancel = findViewById(R.id.b_cancel);

        et_firstName = findViewById(R.id.et_firstName);
        et_lastName = findViewById(R.id.et_lastName);
        et_PN = findViewById(R.id.et_PN);
        et_BusinessEmail = findViewById(R.id.et_BusinessEmail);
        et_pic = findViewById(R.id.et_pic);
        et_businessHours = findViewById(R.id.et_businessHours);
        et_businessWebsite = findViewById(R.id.et_businessWebsite);

        contactList = MyApplication.getTheContactList();
        pa = new PersonAdapter(newBusinessContact.this, contactList);

        b_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstName = et_firstName.getText().toString();
                String lastName = et_lastName.getText().toString();
                String PN = et_PN.getText().toString();
                String BusinessEmail = et_BusinessEmail.getText().toString();
                String Pic = et_pic.getText().toString();
                String BusinessHours = et_businessHours.getText().toString();
                String BusinessWebsite = et_businessWebsite.getText().toString();

                //create new business contact to that holds new fields made
                BusinessContact bc = new BusinessContact(firstName,lastName,PN,BusinessEmail,Pic,BusinessHours,BusinessWebsite);
                //add contact new addressbook
                contactList.getContactList().add(bc);

                DataService ds = new DataService(v.getContext());
                ds.writeList(contactList, "addressBook.txt");

                pa.notifyDataSetChanged();


//                DataService dataService = new DataService(v.getContext());
//                dataService.writeList(ab, "addressBook.txt");

                Intent i = new Intent(v.getContext(), displayContacts.class);
//                i.putExtra("businessFirstName", firstName);
//                i.putExtra("businessLastName", lastName);
//                i.putExtra("businessPN", PN);
//                i.putExtra("businessEmail", BusinessEmail);
//                i.putExtra("businessPic", Pic);
//                i.putExtra("businessHours", BusinessHours);
//                i.putExtra("businessWebsite", BusinessWebsite);

                startActivity(i);
            }
        });

        b_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), MainActivity.class);
                startActivity(i);
            }
        });


    }
}
