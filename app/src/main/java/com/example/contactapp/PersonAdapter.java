package com.example.contactapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class PersonAdapter extends BaseAdapter {

    Activity myActivity;
    AddressBook contactList;
    BaseContact bc;




    //adapter constructor
    public PersonAdapter(Activity myActivity, AddressBook contactList) {
        this.myActivity = myActivity;
        this.contactList = contactList;
    }

    @Override
    //returns how many people are in the list
    public int getCount() {
        return contactList.getContactList().size();
    }

    @Override
    //gets a specific contact
    public BaseContact getItem(int position) {
        return contactList.getContactList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View singleContact;

        LayoutInflater inflater = (LayoutInflater) myActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        singleContact = inflater.inflate(R.layout.single_line_contact, parent , false);

        TextView tv_firstName = singleContact.findViewById(R.id.tv_firstName);
        TextView tv_lastName = singleContact.findViewById(R.id.tv_lastName);
        TextView tv_clickToEdit = singleContact.findViewById(R.id.tv_clickToEdit);

        ImageView iv_picture = singleContact.findViewById(R.id.iv_picture);


        TextView tv_typeOfContact = singleContact.findViewById(R.id.tv_typeOfContact);

        //base contact get position (Contact being displayed)
        bc = this.getItem(position);

        tv_typeOfContact.setText(typeOfContact(bc));

        tv_firstName.setText(bc.getfName());
        tv_lastName.setText(bc.getlName());

        int iconPictures [] = {

                R.drawable.icon01_01,
                R.drawable.icon01_02,
                R.drawable.icon01_03,
                R.drawable.icon01_04,
                R.drawable.icon01_05,
                R.drawable.icon01_06,
                R.drawable.icon01_07,
                R.drawable.icon01_08,
                R.drawable.icon01_09,
                R.drawable.icon01_10,
                R.drawable.icon01_11,
                R.drawable.icon01_12,
                R.drawable.icon01_13,
                R.drawable.icon01_14,
                R.drawable.icon01_15,
                R.drawable.icon01_16,
                R.drawable.icon01_17,
                R.drawable.icon01_18,
                R.drawable.icon01_19,
                R.drawable.icon01_20,
                R.drawable.icon01_21,
                R.drawable.icon01_22,
                R.drawable.icon01_23,
                R.drawable.icon01_24,
                R.drawable.icon01_25,
                R.drawable.icon01_26,
                R.drawable.icon01_27,
                R.drawable.icon01_28,
                R.drawable.icon01_29,
                R.drawable.icon01_30,

        };
        iv_picture.setImageResource(iconPictures[position]);
        return singleContact;
    }

    public String typeOfContact(Object o){

        String answer = "";

        if (o.getClass() == PersonContact.class){
            answer = "Person Contact";
        }
        else{
            answer = "Business Contact";
        }

        return answer;
    }

    public BaseContact getBC(){
        return bc;
    }


}


