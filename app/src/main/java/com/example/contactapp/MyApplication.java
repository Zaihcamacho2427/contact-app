package com.example.contactapp;

import android.app.Application;
import android.service.autofill.Dataset;

import com.example.contactapp.AddressBook;

public class MyApplication extends Application {

        DataService ds = new DataService(MyApplication.this);

    private static AddressBook contactList;

    public static AddressBook getTheContactList() {
        return contactList;
    }

       public void onCreate() {
        super.onCreate();
        contactList = ds.readList("addressBook.txt");

    }
//
    public void onTerminate(){
        super.onTerminate();

//        DataService ds = new DataService(this);
//        ds.writeList(contactList, "addressBook.txt");
    }


}




