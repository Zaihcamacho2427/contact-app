package com.example.contactapp;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BaseContact.class, name = "BaseContact"),
        @JsonSubTypes.Type(value = PersonContact.class, name = "PersonContact"),
        @JsonSubTypes.Type(value = BusinessContact.class, name = "BusinessContact")

})
public abstract class BaseContact {


    protected String fName;
    protected String lName;
    protected String phoneNumber;
    protected String Email;
    protected String imageName;


    //create two base contact constructors. One with parameters and one without
    public BaseContact(String fName, String lName, String phoneNumber, String Email, String imageName) {
        super();
        this.fName = fName;
        this.lName = lName;
        this.phoneNumber = phoneNumber;
        this.Email = Email;
        this.imageName = imageName;
    }
    public BaseContact() {
        this.fName = "Bob";
        this.lName = "Smitty";
        this.phoneNumber = "555-555-5555";
        this.Email = "FakeEmail@fake.com";
        this.imageName = "image.jpg";
    }


    //create compareTo method
    public int compareTo(BaseContact compare) {
        //if both first names and last names match, return 0
        if (this.fName == compare.fName && this.lName == compare.lName) {
            return 0;
        }
        else {
            //if the first names and last names don't match, return -1
            return -1;
        }
    }


    //create getters and setters for all properties
    public String getfName() {
        return fName;
    }
    public void setfName(String fName) {
        this.fName = fName;
    }
    public String getlName() {
        return lName;
    }
    public void setlName(String lName) {
        this.lName = lName;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getEmail() {
        return Email;
    }
    public void setEmail(String email) {
        Email = email;
    }
    public String getImageName() {
        return imageName;
    }
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }




}
