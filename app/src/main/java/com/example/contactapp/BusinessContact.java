package com.example.contactapp;

public class BusinessContact extends BaseContact {

    //create private properties that are specific to business contact
    private String businessHours;
    private String businessWebsite;



    //create two business constructors. One that take in parameters and one that doens't
    public BusinessContact(String fName, String lName, String phoneNumber, String Email, String imageNumber,
                           String businessHours, String businessWebsite){
        super(fName, lName, phoneNumber, Email, imageNumber);
        this.businessHours = businessHours;
        this.businessWebsite = businessWebsite;

    }

    public BusinessContact(){
        super("Dutch ","Bros", "555-555-5555", "Dutchbros@db.com",
                "Dutchbros.jpg");
        this.businessHours = "5:00am - 11:00pm";
        this.businessWebsite = "Dutchbros.com";
    }

    //create compare to method similar to BaseContact comapreTo method
    public int compareTo(BusinessContact p) {
        if (this.fName == p.fName && this.lName == p.lName) {
            return 0;
        }
        else {
            return -1;
        }

    }

}
