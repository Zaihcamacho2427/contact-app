package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditPersonContact extends AppCompatActivity {

    Button b_edit, b_mainMenu, b_delete;
    EditText et_firstName;
    EditText et_lastName;
    EditText et_PN;
    EditText et_PersonEmail;
    EditText et_pic;
    EditText et_nickname;
    EditText et_PersonBirthday;
    int positionToEdit = -1;
    AddressBook contactList;
    PersonAdapter pa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_person_contact);

        b_edit = findViewById(R.id.b_edit);
        b_mainMenu = findViewById(R.id.b_mainMenu);
        b_delete = findViewById(R.id.b_delete);
        et_firstName = findViewById(R.id.et_firstName);
        et_lastName = findViewById(R.id.et_lastName);
        et_PN = findViewById(R.id.et_PN);
        et_PersonEmail = findViewById(R.id.et_PersonEmail);
        et_pic = findViewById(R.id.et_pic);
        et_nickname = findViewById(R.id.et_nickname);
        et_PersonBirthday = findViewById(R.id.et_PersonBirthday);

        contactList = MyApplication.getTheContactList();

        pa = new PersonAdapter(EditPersonContact.this, contactList);

        //capture incoming info sent from another form
        Bundle incomingPInfo = getIntent().getExtras();

        if (incomingPInfo != null){
            String firstName = incomingPInfo.getString("fName");
            String lName = incomingPInfo.getString("lName");
            String pPN = incomingPInfo.getString("pPN");
            String pEmail = incomingPInfo.getString("pEmail");
            String pPic = incomingPInfo.getString("pPic");
            //store the value of the position of clicked contact into new int variable
            positionToEdit = incomingPInfo.getInt("edit");

            //set the text views to the values of the person being edited
            et_firstName.setText(firstName);
            et_lastName.setText(lName);
            et_PN.setText(pPN);
            et_PersonEmail.setText(pEmail);
            et_pic.setText(pPic);

        }

        b_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newFirstName = et_firstName.getText().toString();
                String newLastName = et_lastName.getText().toString();
                String newPN = et_PN.getText().toString();
                String newPEmail = et_PersonEmail.getText().toString();
                String newPPic = et_pic.getText().toString();
                String newPNickname = et_nickname.getText().toString();
                String newPBirthday = et_PersonBirthday.getText().toString();
                //create new person using fields entered in by user
                PersonContact p = new PersonContact(newFirstName, newLastName, newPN, newPEmail, newPPic, newPNickname, newPBirthday);
                //remove old version of contact from the address book using position passed to this form
//                ab.getContactList().remove(positionToEdit);
                contactList.getContactList().remove(positionToEdit);
                //add new (edited) person to the address book and notify to the adapter that something has changed
                contactList.addContact(p);

                //call data service and add new person created to the addressBook.txt file
                DataService ds = new DataService(v.getContext());
                ds.writeList(contactList, "addressBook.txt");
                //tell adapter that something has changed in the address book.
                pa.notifyDataSetChanged();
                //make intent to go to displayContact page
                Intent i = new Intent(v.getContext(), displayContacts.class);
                //send an extra with int holding position of contact clicked
                startActivity(i);


            }
        });

        b_mainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), MainActivity.class);
                startActivity(i);
            }
        });

        b_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), deletePQuestion.class);
                i.putExtra("deletePContact", positionToEdit);
                startActivity(i);
            }
        });


    }
}
