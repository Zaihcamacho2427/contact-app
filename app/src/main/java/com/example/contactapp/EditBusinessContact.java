package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditBusinessContact extends AppCompatActivity {



    Button b_edit, b_mainMenu, b_delete;
    EditText et_firstName;
    EditText et_lastName;
    EditText et_PN;
    EditText et_BusinessEmail;
    EditText et_pic;
    EditText et_businessHours;
    EditText et_businessWebsite;
    AddressBook contactList;
    int positionToEdit2 = -1;
    PersonAdapter pa;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_business_contact);

        b_edit= findViewById(R.id.b_edit);
        b_mainMenu = findViewById(R.id.b_mainMenu);
        b_delete = findViewById(R.id.b_delete);

        et_firstName = findViewById(R.id.et_firstName);
        et_lastName = findViewById(R.id.et_lastName);
        et_PN = findViewById(R.id.et_PN);
        et_BusinessEmail = findViewById(R.id.et_BusinessEmail);
        et_pic = findViewById(R.id.et_pic);
        et_businessHours = findViewById(R.id.et_businessHours);
        et_businessWebsite = findViewById(R.id.et_businessWebsite);

        contactList = MyApplication.getTheContactList();

        pa = new PersonAdapter(EditBusinessContact.this, contactList);


        Bundle incomingBInfo = getIntent().getExtras();

        if (incomingBInfo != null){
            String firstName = incomingBInfo.getString("fName");
            String lastName = incomingBInfo.getString("lName");
            String bPN = incomingBInfo.getString("bPN");
            String bEmail = incomingBInfo.getString("bEmail");
            String bPic = incomingBInfo.getString("bPic");
            positionToEdit2 = incomingBInfo.getInt("edit2");

            et_firstName.setText(firstName);
            et_lastName.setText(lastName);
            et_PN.setText(bPN);
            et_BusinessEmail.setText(bEmail);
            et_pic.setText(bPic);


        }

        b_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newFirstName = et_firstName.getText().toString();
                String newLastName = et_lastName.getText().toString();
                String newPN = et_PN.getText().toString();
                String newEmail = et_BusinessEmail.getText().toString();
                String newPic = et_pic.getText().toString();
                String newBHours = et_businessHours.getText().toString();
                String newBWebsite = et_businessWebsite.getText().toString();

                BusinessContact bc = new BusinessContact(newFirstName, newLastName, newPN, newEmail, newPic, newBHours, newBWebsite);
                contactList.getContactList().remove(positionToEdit2);
                contactList.addContact(bc);

                DataService ds = new DataService(v.getContext());
                ds.writeList(contactList, "addressBook.txt");

                pa.notifyDataSetChanged();




                Intent i = new Intent(v.getContext(), displayContacts.class);
                startActivity(i);
            }
        });

        b_mainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), MainActivity.class);
                startActivity(i);
            }
        });

        b_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), deleteBQuestion.class);
                i.putExtra("deleteBContact", positionToEdit2);
                startActivity(i);
            }
        });
    }
}
