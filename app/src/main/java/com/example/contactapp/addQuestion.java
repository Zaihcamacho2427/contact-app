package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class addQuestion extends AppCompatActivity {

    Button b_person, b_business;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_question);

        b_person = findViewById(R.id.b_person);
        b_business= findViewById(R.id.b_business);

        b_person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), newPersonContact.class);
                startActivity(i);
            }
        });

        b_business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(v.getContext(), newBusinessContact.class);
                startActivity(i2);
            }
        });
    }
}
