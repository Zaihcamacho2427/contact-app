package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class deletePQuestion extends AppCompatActivity {

    Button b_yes;
    Button b_no;
    TextView tv_question;
    AddressBook contactList;
    Bundle contact;
    PersonAdapter pa;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_pquestion);


        b_yes = findViewById(R.id.b_yes);
        b_no = findViewById(R.id.b_no);
        tv_question = findViewById(R.id.tv_question);

//        connect address book to existing address book using MyApplication
        contactList = MyApplication.getTheContactList();

        pa = new PersonAdapter(deletePQuestion.this, contactList);

        //create bundle to look for position of person clicked
        contact = new Bundle(getIntent() .getExtras());
        pos = contact.getInt("deletePContact");

        b_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if bundle isn't empty in order to delete contact clicked
                if (pos >= 0){

                    contactList = MyApplication.getTheContactList();
                    contactList.getContactList().remove(pos);

                    DataService ds = new DataService(v.getContext());
                    ds.writeList(contactList, "addressBook.txt");
                    pa.notifyDataSetChanged();

                    // let user know contact wasn't deleted
                    Toast.makeText(deletePQuestion.this, "Person contact was deleted.", Toast.LENGTH_SHORT).show();
                    //go to home page
                    Intent i = new Intent(v.getContext(), MainActivity.class);
                    startActivity(i);
                }

            }
        });
        b_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(deletePQuestion.this, "Person contact NOT deleted.", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(v.getContext(), MainActivity.class);
                startActivity(i);
            }
        });
    }
}
