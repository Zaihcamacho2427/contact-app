package com.example.contactapp;

public class PersonContact extends BaseContact {

    private String nickname;
    private String birthday;

    //public constructor using no parameters
    public PersonContact() {
        super("Bob", "Smitty", "541-660-5345",
                "Zaihcamacho41@yahoo.com", "fakeImage.jpg");
        this.nickname = "Bobby";
        this.birthday = "9/27/1997";

    }

    //public constructor using parameters
    public PersonContact(String fName, String lName, String phoneNumber, String Email, String imageName,
                         String nickname, String birthday) {
        super(fName, lName, phoneNumber, Email, imageName);
        this.nickname = nickname;
        this.birthday = birthday;
    }


    public String getNickname() {
        return nickname;
    }


    public void setNickname(String nickname) {
        this.nickname = nickname;
    }


    public String getBirthday() {
        return birthday;
    }


    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

//    //create toString method
//    public String toString() {
//        return
//                "------------------Person Contact--------------------------------------------" + "\n" +
//                        "First name: " + this.fName + "\n" +
//                        "Last name: " + this.lName + "\n" +
//                        "Phone number: " + this.phoneNumber + "\n" +
//                        "Email: " + this.Email +  "\n" +
//                        "Image name: " + this.imageName + "\n" +
//                        "Nickname: " + this.nickname + "\n" +
//                        "DOB: " + this.birthday + "\n" +
//                        "---------------------------------------------------------------------" + "\n";
//    }


    //create compareTo method similar to BaseContact compareTo method
    public int compareTo(PersonContact p) {
        if (this.fName == p.fName && this.lName == p.lName) {
            return 0;
        }
        else {
            return -1;
        }

    }




}
