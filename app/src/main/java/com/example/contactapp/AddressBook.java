package com.example.contactapp;

import android.provider.Telephony;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AddressBook {

    private static Scanner sc = new Scanner(System.in);


    //address book is the main data storage structure for the entire app.
    //this class will demonstrate the use of generics

    //create private list that holds all the base contacts
    private List<BaseContact> contactList;


    //create constructor that holds base contact list and has contactList as parameter
    public AddressBook(List<BaseContact> contactList) {
        this.contactList = contactList;
    }

    //create constructor that holds 1 PersonContact and 1 BusinessContact and at them to an empty array list
    public AddressBook(){
        this.contactList = new ArrayList<>();
//        PersonContact pc = new PersonContact();
//        BusinessContact bc = new BusinessContact();
//        contactList.add(pc);
//        contactList.add(bc);


    }

    //create boolean with string parameter that returns contact with first name or last name in address book
    public boolean searchList(String name) {

        for (int i = 0; i < contactList.size(); i++) {
            if (contactList.get(i).getfName().equals(name)) {
                return true;
            }
            else if (contactList.get(i).getlName().equals(name)) {
                return true;
            }
            else if (contactList.get(i).getPhoneNumber().equals(name)) {
                return true;
            }
        }
        return false;
    }

    //	create method that adds one person/business contact to "contactList using t generic"
//	using generic helps reduce repeating code
    public void  addContact (BaseContact bc) {
        contactList.add(bc);
    }

    //create method that searches for a contact using first/last name string parameter.
    public AddressBook searchForContact(String string) {
        //create new array list using Base Contact that will store possible results
        AddressBook results = new AddressBook();

        for (int i = 0; i < contactList.size(); i++) {
            //if contact address book contains user searched first name string
            if (contactList.get(i).getlName().contains(string)) {
                results.getContactList().add(contactList.get(i));
                //if result, enter result into new array list
            }
            // if contact address book contains user searched last name string
            else if (contactList.get(i).getfName().contains(string)){
                results.getContactList().add(contactList.get(i));
            }
        }
        //print results array holding found contacts
        return results;
    }
    //
    //method to get contact location from addressbook
    public BaseContact getContact(int i) {
        return contactList.get(i);
    }
//
//    //method to sort contact list
//    public void sort() {
//        Collections.sort(contactList);
//    }
//    method to return contact list size.
   @JsonIgnore
    public int getListSize() {
        return contactList.size();
    }
    //method to display contact name within address book
    public void displayContact(BaseContact c) {
        for (int i = 0; i < contactList.size(); i++) {
            if (contactList.get(i).equals(c)) {
                System.out.println("Contact found: " + contactList.get(i).getfName());
            }
        }
    }

    public void deleteContact(String firstName, String lastName)
    {
        //for loop going through contactList
        for (int i = 0; i < contactList.size(); i++)
        {
            //if contactList has contact that user entered using first name or last name
            if (contactList.get(i).getfName().equals(firstName) ||
                    contactList.get(i).getlName().equals(lastName))
            {

                contactList.remove(contactList.get(i));
            }

        }

    }
    public void deleteContactFName(String fName) {
        for (int i = 0; i < contactList.size(); i++) {
            if (contactList.get(i).getfName().equals(fName)) {
                contactList.remove(contactList.get(i));
            }
        }
    }

    public void displayAllContacts(AddressBook CL) {
        for (int i = 0; i < contactList.size(); i++) {
            System.out.println(contactList.get(i));
        }
    }

    public void text(String name)
    {
        String input2;
        System.out.println("Texting: ");
        System.out.println("What would you like to say?");
        input2 = sc.nextLine();
        System.out.println(input2 + " has been sent");

    }

    public void call(String user2)
    {
        System.out.println("Calling: ");

    }

    public void email(String userEmail)
    {
        String input2;
        System.out.println("Email: ");
        System.out.println("What would you like to say?");
        input2 = sc.nextLine();
        System.out.println(input2 + " has been sent to");

    }

    public void editPersonalContact(String personFName, String personLName) {
        for (int i = 0; i < contactList.size(); i++) {
            if (contactList.get(i).getfName().equals(personFName) && contactList.get(i).getlName().equals(personLName)) {

            }
        }
    }

    public int editableContacts(String search) {
        int position = -1;
        int countContainMatch = 0;
        int countExactMatch = 0;
        if(searchList(search) == true) {
            for (int i = 0; i < contactList.size(); i++) {
                if (contactList.get(i).getfName().contains(search) || contactList.get(i).getlName().contains(search)) {
                    position = i;
                    countContainMatch++;
                }
            }
            if (countContainMatch > 1) {
                System.out.println("There are multiple contacts with that first name or last name, enter in first/last name as precise as you can");
                System.out.println("Or type 0 to cancel search");
                if (contactList.get(position).getfName().contains(search)) {
                    search = contactList.get(position).getfName();
                }
                else if (contactList.get(position).getlName().contains(search)){
                    search = contactList.get(position).getlName();
                }
                String search2;
                search2 = sc.nextLine();
                boolean stopSearch = false;
                int secondSearch = 0;
                while(stopSearch == false && !search2.equals("0") && secondSearch < contactList.size() -1) {
                    if (secondSearch == 0) {
                        System.out.println("No match, sorry!");
                    }
                    for (int i = 0; i < contactList.size(); i++) {
                        if (contactList.get(i).getfName().equals(search2)) {
                            position = i;
                            stopSearch = true;
                            countExactMatch++;
                        }
                        else if (contactList.get(i).getlName().equals(search2)) {
                            position = i;
                            stopSearch = true;
                            countExactMatch++;
                        }
                    }
                    secondSearch++;
                }
                if (search2.equals("0")) {
                    position = -1;
                }
                else if (secondSearch >= contactList.size() -1) {
                    position = -2;
                }

                if (countExactMatch > 1) {
                    position = -2;

                }
            }

        }
        return position;

    }

    public List<BaseContact> getContactList() {
        return contactList;
    }

    public void setContactList(List<BaseContact> contactList) {
        this.contactList = contactList;
    }


}
