package com.example.contactapp;

import android.content.Context;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabWidget;
import android.widget.TextView;

public class newPersonContact extends AppCompatActivity {


    Button b_add, b_cancel;
    EditText et_firstName;
    EditText et_lastName;
    EditText et_PN;
    EditText et_PersonEmail;
    EditText et_pic;
    EditText et_nickname;
    EditText et_PersonBirthday;
    AddressBook contactList;
    PersonAdapter pa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_person_contact);

        b_add = findViewById(R.id.b_add);
        b_cancel = findViewById(R.id.b_cancel);

        //connect addressbook to previous addressbook
        contactList = MyApplication.getTheContactList();

        pa = new PersonAdapter(newPersonContact.this, contactList);

        et_firstName = findViewById(R.id.et_firstName);
        et_lastName = findViewById(R.id.et_lastName);
        et_PN = findViewById(R.id.et_PN);
        et_PersonEmail = findViewById(R.id.et_PersonEmail);
        et_pic = findViewById(R.id.et_pic);
        et_nickname = findViewById(R.id.et_nickname);
        et_PersonBirthday = findViewById(R.id.et_PersonBirthday);



        b_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            String firstName = et_firstName.getText().toString();
            String lastName = et_lastName.getText().toString();
            String PN = et_PN.getText().toString();
            String PersonEmail = et_PersonEmail.getText().toString();
            String Pic = et_pic.getText().toString();
            String nickname = et_nickname.getText().toString();
            String PersonBirthday = et_PersonBirthday.getText().toString();


            //create new person with fields just made
            PersonContact p = new PersonContact(firstName, lastName,PN, PersonEmail, Pic, nickname, PersonBirthday);
            //store person into address book
                contactList.getContactList().add(p);

                //call the data service so you can add the updated address book with new contact created
                DataService ds = new DataService(v.getContext());
                //write to text file "addressBook.txt"
                ds.writeList(contactList, "addressBook.txt");
                //notify to the adapter that it has been changed
                pa.notifyDataSetChanged();

//            DataService ds = new DataService(v.getContext());
//            ds.writeList(ab, "addressBook.txt");

            //create intent to go to displayContact page
                Intent i = new Intent(v.getContext(), displayContacts.class);
//                i.putExtra("personFirstName", firstName);
//                i.putExtra("personLastName", lastName);
//                i.putExtra("personPN", PN);
//                i.putExtra("personEmail", PersonEmail);
//                i.putExtra("personPic", Pic);
//                i.putExtra("nickname", nickname);
//                i.putExtra("personBirthday", PersonBirthday);

                startActivity(i);
            }
        });
        b_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), MainActivity.class);
                startActivity(i);
            }
        });


    }
}
