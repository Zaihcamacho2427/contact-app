//package com.example.contactapp;
//
//import android.content.Intent;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.ListView;
//
//public class displaySearchedResults extends AppCompatActivity {
//
//    ListView lv_results;
//    PersonAdapter adapter;
//    AddressBook fakeAddressBook;
//    AddressBook results;
//    BaseContact bc;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_display_searched_results);
//
//        //create list view
//        lv_results = findViewById(R.id.lv_results);
//
//
//        fakeAddressBook = ((MyApplication) this.getApplication()) .getTheContactList();
//
//        results = fakeAddressBook;
//
//        //create bundle to read in address book sent by previous form.
//        Bundle bundle = getIntent().getExtras();
//        if (bundle != null){
//            for (int i = 0; i < results.getListSize(); i++) {
//                String firstName = bundle.getString("fName");
//                String lName = bundle.getString("lName");
//                String pPN = bundle.getString("PN");
//                String pEmail = bundle.getString("Email");
//                String pPic = bundle.getString("Pic");
//                bc = new BaseContact(firstName, lName, pPN, pEmail, pPic);
//
//
//            }
//        }
//
//
//
//        results = ((MyApplication) this.getApplication()) .getTheContactList();
//
//        adapter = new PersonAdapter(displaySearchedResults.this, results);
//
//        lv_results.setAdapter(adapter);
//
//        lv_results.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                editPerson(position);
//            }
//        });
//
//
//    }
//
//    public void editPerson(int position){
//
//        BaseContact bc = results.getContactList().get(position);
//
//        if (bc.getClass() == PersonContact.class) {
//            Intent i = new Intent(getApplicationContext(), EditPersonContact.class);
//
//            i.putExtra("edit", position);
//            i.putExtra("fName", bc.getfName());
//            i.putExtra("lName", bc.getlName());
//            i.putExtra("pPN", bc.getPhoneNumber());
//            i.putExtra("pEmail", bc.getEmail());
//            i.putExtra("pPic", bc.getImageName());
//            startActivity(i);
//        }
//        else if (bc.getClass() == BusinessContact.class) {
//
//            Intent i = new Intent(getApplicationContext(), EditBusinessContact.class);
//            i.putExtra("edit2", position);
//            i.putExtra("fName", bc.getfName());
//            i.putExtra("lName", bc.getlName());
//            i.putExtra("bPN", bc.getPhoneNumber());
//            i.putExtra("bEmail", bc.getEmail());
//            i.putExtra("bPic", bc.getImageName());
//            startActivity(i);
//        }
//
//
//    }
//}
